'use strict';

function initCatala() {
  console.log('Google Maps API initialized.');

  angular.bootstrap(document.getElementById('map'), ['catala']);
}

angular
  .module('catala', ['google-maps'])

  .controller('CatalaController', function($scope){

  })

  .controller('MapController', ['$scope', function($scope){
        $scope.map = {
          center: {
            latitude: -5.8889506, longitude: -42.637341
          },

          zoom: 14,

          bounds: {}
        };

        $scope.options = {scrollwheel: false};

        $scope.markersEvents = {
          click: function (gMarker, eventName, model) {
            if(model.$id){
              model = model.coords;// use scope portion then
            }

          }
        };

        $scope.markers = [];
        // Get the bounds from the map once it's loaded
        $scope.$watch(function() { return $scope.map.bounds; }, function(nv, ov) {
            if (!ov.southwest && nv.southwest) {
              $scope.markers = [
                {
                  id: 0,
                  name: 'Comercial Carlos',
                  latitude: -5.8936202,
                  longitude: -42.6451526
                }
              ];
            }
        }, true);
  }]);
